def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, times = 2)
  repeat_str = Array.new(times, str)
  repeat_str.join(" ")
end

def start_of_word(str, number)
  str[0...number]
end

def first_word(str)
  str.split[0]
end

def titleize(str)
  small_words = ["over", "the", "and"]
  split_str = str.split
  split_str.map.with_index do |word, idx|
    if idx > 0 && small_words.include?(word)
      word
    else
      word.capitalize
    end
  end.join(" ")
end
