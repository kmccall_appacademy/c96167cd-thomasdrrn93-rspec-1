def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  if arr.empty?
    return 0
  end
  arr.reduce(:+)
end

def multiply(*numbers)
  if numbers.empty?
    return 0
  end
  numbers.reduce(:*)
end

def power(base, exponent)
  base**exponent
end

def factorial(num)
  return 1 if num == 0
  (1..num).reduce(:*)
end
