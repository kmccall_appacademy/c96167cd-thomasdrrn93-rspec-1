def translate(str)
  str.split.map { |word| translate_word(word) }.join(" ")
end

def translate_word(word)
  if "aeiou".include?(word[0])
    "#{word}ay"
  else
    phoneme_end = 0
    until "aeiou".include?(word[phoneme_end])
      phoneme_end += 1
    end
    phoneme_end += 1 if word[phoneme_end - 1] == "q"
    "#{word[phoneme_end..-1]}#{word[0...phoneme_end]}ay"
  end
end
